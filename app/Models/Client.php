<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    protected $fillable = [
        'full_name',
        'uin',
        'сontract_number',
        'phone_number',
        'birth_date',
        'email',
        'user_id',
    ];
}
