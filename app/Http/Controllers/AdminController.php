<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function get_index()
    {
        if (Auth::user()->client) {
            return view('pages/index');
        }

        return view('pages/admin/index', [
            'last_clients' => DB::table('clients')->orderBy('id', 'desc')->limit(15)->get(),
        ]);
    }

    public function get_client_create()
    {
        if (Auth::user()->client) {
            return abort(404);
        }

        return view('pages/admin/client/create');
    }

    public function get_client_view($id)
    {
        if (Auth::user()->client) {
            return abort(404);
        }

        $item = Client::findOrFail($id);
        return view('pages/admin/client/view', ['item' => $item]);
    }

    public function get_client_delete($id)
    {
        if (Auth::user()->client) {
            return abort(404);
        }

        $item = Client::findOrFail($id);

        User::destroy($item->user_id);

        $item->delete();

        return redirect('/client')->with('success_message', 'Клиент успешно удалено!');
    }

    public function post_client_create(Request $request)
    {
        if (Auth::user()->client) {
            return abort(404);
        }

        $validator = Validator::make($request->all(), [
            'full_name' => 'required|string|max:255',
            'uin' => 'required|string|max:255',
            'сontract_number' => 'required|integer',
            'phone_number' => 'required|string|max:255',
            'birth_date' => 'required|date',
            'email' => 'required|email|max:255|unique:users',
            'contributed_amount' => 'required|integer',
            'remainder' => 'required|integer',
            'password' => 'required|string|min:8|confirmed',
        ]);

        if ($validator->fails()) return back()->withErrors($validator)->withInput();

        $item = new Client();
        $user = new User();

        $user->name = $request->get('full_name');
        $user->email = $request->get('email');
        $user->password = Hash::make($request->get('password'));

        $user->save();

        $item->full_name = $request->get('full_name');
        $item->uin = $request->get('uin');
        $item->сontract_number = $request->get('сontract_number');
        $item->phone_number = $request->get('phone_number');
        $item->birth_date = $request->get('birth_date');
        $item->email = $request->get('email');
        $item->contributed_amount = $request->get('contributed_amount');
        $item->remainder = $request->get('remainder');
        $item->creator_id = Auth::id();
        $item->user_id = $user->id;

        $item->save();

        return redirect('/client')->with('success_message', 'Новый клиент успешно добавлено!');
    }

    public function post_client_view(Request $request, $id)
    {
        if (Auth::user()->client) {
            return abort(404);
        }

        $validator = Validator::make($request->all(), [
            'full_name' => 'required|string|max:255',
            'uin' => 'required|string|max:255',
            'сontract_number' => 'required|integer',
            'phone_number' => 'required|string|max:255',
            'birth_date' => 'required|date',
            //'email' => 'required|email|max:255',
            'contributed_amount' => 'required|integer',
            'remainder' => 'required|integer',
        ]);

        $item = Client::findOrFail($id);

        if ($validator->fails()) return back()->withErrors($validator)->withInput();

        $item->full_name = $request->get('full_name');
        $item->uin = $request->get('uin');
        $item->сontract_number = $request->get('сontract_number');
        $item->phone_number = $request->get('phone_number');
        $item->birth_date = $request->get('birth_date');
        //$item->email = $request->get('email');
        $item->contributed_amount = $request->get('contributed_amount');
        $item->remainder = $request->get('remainder');
        $item->creator_id = Auth::id();

        $item->save();

        return back()->with('success_message', 'Клиент успешно редактировано!');
    }

    public function get_client_index()
    {
        if (Auth::user()->client) {
            return abort(404);
        }

        return view('pages/admin/client/index', [
            'items' => DB::table('clients')->orderBy('id', 'desc')->paginate(15),
        ]);
    }
}
