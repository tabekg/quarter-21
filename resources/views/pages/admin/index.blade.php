@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Личный кабинет') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        Добро пожаловать, {{ \Illuminate\Support\Facades\Auth::user()->name }}!
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center mt-4">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <span>{{ __('Недавние добавленные клиенты') }}</span>
                        <a href="/client/create" class="btn btn-primary btn-sm">Новый клиент</a>
                    </div>

                    @if(count($last_clients) > 0)
                        <table class="table mb-0 table-striped">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">ФИО</th>
                                <th scope="col">ИИН</th>
                                <th scope="col">Номер договора</th>
                                <th scope="col">Номер телефона</th>
                                <th scope="col">Дата рождения</th>
                                <th scope="col">E-Mail адрес</th>
                                <th scope="col">Внесенная сумма в кооператив</th>
                                <th scope="col">Остаток</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($last_clients as $item)
                                <tr class="clickable-row" data-href="/client/{{$item->id}}">
                                    <th scope="row">{{$item->id}}</th>
                                    <td>{{$item->full_name}}</td>
                                    <td>{{$item->uin}}</td>
                                    <td>{{$item->сontract_number}}</td>
                                    <td>{{$item->phone_number}}</td>
                                    <td>{{$item->birth_date}}</td>
                                    <td>{{$item->email}}</td>
                                    <td>{{$item->contributed_amount}}</td>
                                    <td>{{$item->remainder}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="text-center m-5 text-muted">
                            <div>Список пуст.</div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
