@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @if (session('success_message'))
                    <div class="alert alert-success">
                        {{ session('success_message') }}
                    </div>
                @endif

                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <span>Клиенты ({{\App\Models\Client::count()}})</span>
                        <a href="/client/create" class="btn btn-primary btn-sm">Новый клиент</a>
                    </div>

                    <div>
                        @if(count($items) > 0)
                            <table class="table mb-0 table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">ФИО</th>
                                    <th scope="col">ИИН</th>
                                    <th scope="col">Номер договора</th>
                                    <th scope="col">Номер телефона</th>
                                    <th scope="col">Дата рождения</th>
                                    <th scope="col">E-Mail адрес</th>
                                    <th scope="col">Внесенная сумма в кооператив</th>
                                    <th scope="col">Остаток</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($items as $item)
                                    <tr class="clickable-row" data-href="/client/{{$item->id}}">
                                        <th scope="row">{{$item->id}}</th>
                                        <td>{{$item->full_name}}</td>
                                        <td>{{$item->uin}}</td>
                                        <td>{{$item->сontract_number}}</td>
                                        <td>{{$item->phone_number}}</td>
                                        <td>{{$item->birth_date}}</td>
                                        <td>{{$item->email}}</td>
                                        <td>{{$item->contributed_amount}}</td>
                                        <td>{{$item->remainder}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            {{$items->links()}}
                        @else
                            <div class="text-center m-5 text-muted">
                                <div>Список пуст.</div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
