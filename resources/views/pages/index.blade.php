@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Личный кабинет') }}</div>

                    <div class="card-body">
                        <div class="mb-2">Добро пожаловать, {{ \Illuminate\Support\Facades\Auth::user()->name }}!</div>

                        <h3>Внесенная сумма в
                            кооператив: {{\Illuminate\Support\Facades\Auth::user()->client->contributed_amount}}</h3>
                        <h3>Остаток: {{\Illuminate\Support\Facades\Auth::user()->client->remainder}}</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
