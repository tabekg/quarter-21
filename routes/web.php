<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [App\Http\Controllers\AdminController::class, 'get_index']);
Route::get('/client', [App\Http\Controllers\AdminController::class, 'get_client_index']);

Route::get('/client/create', [App\Http\Controllers\AdminController::class, 'get_client_create']);
Route::post('/client/create', [App\Http\Controllers\AdminController::class, 'post_client_create']);

Route::get('/client/{id}', [App\Http\Controllers\AdminController::class, 'get_client_view'])->where('id', '[0-9]+');
Route::post('/client/{id}', [App\Http\Controllers\AdminController::class, 'post_client_view'])->where('id', '[0-9]+');

Route::get('/client/delete/{id}', [App\Http\Controllers\AdminController::class, 'get_client_delete'])->where('id', '[0-9]+');
